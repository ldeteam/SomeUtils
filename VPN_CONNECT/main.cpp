#include <atlstr.h>
#include <windows.h>
#include <windowsx.h>
#include <winuser.h>

#include <document.h>
#include <writer.h>
#include <stringbuffer.h>

#include <future>
#include <iostream>
#include <string>
#include <fstream>

using namespace rapidjson;

auto const VIP_ACCESS_WINDOW_NAME = L" VIP Access";
auto const VIP_ACCESS_COPY_BTN_WINDOW_NAME = L"Copy Security code";
auto const ANYCONNECT_WINDOW_NAME = L"Cisco AnyConnect Secure Mobility Client";
auto const ANYCONNECT_CONNECT_TRIGGER_BTN_WINDOW_NAME = L"Connect";
auto const ANYCONNECT_CONNECT_WINDOW_NAME =
    L"Cisco AnyConnect | access.here.com";

auto const anyconnect_config_dir = "C:\\ProgramData\\Cisco\\Cisco AnyConnect Secure Mobility Client\\Profile";
auto const anyconnect_modified_config = "C:\\ProgramData\\Cisco\\Cisco AnyConnect Secure Mobility Client\\Profile\\custom_HERE.xml";
auto const anyconnect_origin_config = "C:\\ProgramData\\Cisco\\Cisco AnyConnect Secure Mobility Client\\Profile\\HERE.xml";

auto const config_filename = "config.bin";
auto const secret_key = "markus";

auto const param_anyconnect_executable = "anyconnect_executable";
auto const param_vipaccess_executable = "vipaccess_executable";
auto const param_username = "username";
auto const param_userpin = "userpin";

enum class ConnType { VIP_TOKEN, TUNNEL_ALL };

HWND vip_access_wnd = nullptr;
HWND vip_access_copy_btn_wnd = nullptr;
HWND anyconnect_wnd = nullptr;
HWND anyconnect_connect_wnd = nullptr;
HWND vpn_group_wnd = nullptr;
HWND vpn_username_wnd = nullptr;
HWND vpn_password_wnd = nullptr;
HWND vpn_connect_btn_wnd = nullptr;
unsigned enum_proc_wnd_cnt = 0;

BOOL CALLBACK MyEnumProc(HWND hWnd, LPARAM lParam) {
  if (enum_proc_wnd_cnt == 3) {
    vpn_group_wnd = hWnd;
  }

  if (enum_proc_wnd_cnt == 5) {
    vpn_username_wnd = hWnd;
  }

  if (enum_proc_wnd_cnt == 7) {
    vpn_password_wnd = hWnd;
  }

  if (enum_proc_wnd_cnt == 8) {
    vpn_connect_btn_wnd = hWnd;
  }

  enum_proc_wnd_cnt++;

  return TRUE;
}

HWND find_window(const HWND hWndParent, const HWND hWndChildAfter,
                 const LPCWSTR lpszClass, const LPCWSTR lpszWindow,
                 int timeout = 10) {
  HWND hwnd = nullptr;
  timeout *= 10;
  while (hwnd == nullptr && timeout-- > 0) {
    hwnd = (hWndParent != nullptr || hWndChildAfter != nullptr)
               ? FindWindowEx(hWndParent, hWndChildAfter, lpszClass, lpszWindow)
               : FindWindow(lpszClass, lpszWindow);
    Sleep(100);
  }

  return hwnd;
}

HWND get_window(const HWND hWnd, const UINT uCmd, int timeout = 10) {
  HWND target_hwnd = nullptr;
  timeout *= 10;
  while (target_hwnd == nullptr && timeout-- > 0) {
    target_hwnd = GetWindow(hWnd, uCmd);
    Sleep(100);
  }

  return target_hwnd;
}

int find_anyconnect_connect_window() {

  anyconnect_connect_wnd =
      find_window(nullptr, nullptr, nullptr, ANYCONNECT_CONNECT_WINDOW_NAME);
  if (anyconnect_connect_wnd == nullptr) {
    return 1;
  }

  vpn_group_wnd = nullptr;
  vpn_username_wnd = nullptr;
  vpn_password_wnd = nullptr;
  vpn_connect_btn_wnd = nullptr;
  enum_proc_wnd_cnt = 0;
  EnumChildWindows(anyconnect_connect_wnd, MyEnumProc, NULL);

  if (vpn_group_wnd == nullptr || vpn_username_wnd == nullptr ||
      vpn_password_wnd == nullptr || vpn_connect_btn_wnd == nullptr) {
    return 1;
  }

  return 0;
}

int find_all_required_windows() {
  vip_access_wnd =
      find_window(nullptr, nullptr, nullptr, VIP_ACCESS_WINDOW_NAME);
  if (vip_access_wnd == nullptr) {
    std::cerr << "VIP ACCESS main window was not found!" << std::endl;
    return 1;
  }

  anyconnect_wnd =
      find_window(nullptr, nullptr, nullptr, ANYCONNECT_WINDOW_NAME);
  if (anyconnect_wnd == nullptr) {
    std::cerr << "ANYCONNECT main window was not found!" << std::endl;
    return 1;
  }

  vip_access_copy_btn_wnd = find_window(vip_access_wnd, nullptr, nullptr,
                                        VIP_ACCESS_COPY_BTN_WINDOW_NAME);
  if (vip_access_copy_btn_wnd == nullptr) {
    std::cerr << "VIP ACCESS copy button was not found!" << std::endl;
    return 1;
  }

  auto const anyconnect_top_child_wnd = get_window(anyconnect_wnd, GW_CHILD);
  if (anyconnect_top_child_wnd == nullptr) {
    std::cerr << "ANYCONNECT top child window was not found!" << std::endl;
    return 1;
  }

  auto const anyconnect_connect_wnd_trigger_btn_wnd =
      find_window(anyconnect_top_child_wnd, nullptr, nullptr,
                  ANYCONNECT_CONNECT_TRIGGER_BTN_WINDOW_NAME);
  if (anyconnect_connect_wnd_trigger_btn_wnd == nullptr) {
    std::cerr << "ANYCONNECT connect window trigger button was not found!"
              << std::endl;
    return 1;
  }

  SendMessage(anyconnect_connect_wnd_trigger_btn_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(anyconnect_connect_wnd_trigger_btn_wnd, WM_LBUTTONUP, 0, 0);

  if (find_anyconnect_connect_window() != 0) {
    std::cerr << "ANYCONNECT connect window was not found!" << std::endl;
    return 1;
  }

  return 0;
}

void click_password_copy_btn() {
  SendMessage(vip_access_copy_btn_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(vip_access_copy_btn_wnd, WM_LBUTTONUP, 0, 0);
  SendMessage(vip_access_copy_btn_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(vip_access_copy_btn_wnd, WM_LBUTTONUP, 0, 0);
}

void click_connect_btn() {
  SendMessage(vpn_connect_btn_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(vpn_connect_btn_wnd, WM_LBUTTONUP, 0, 0);
  SendMessage(vpn_connect_btn_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(vpn_connect_btn_wnd, WM_LBUTTONUP, 0, 0);
}

std::string get_vpn_password(const std::string &pin) {
  Sleep(100);

  if (!OpenClipboard(nullptr)) {
    return {};
  }

  auto const hData = GetClipboardData(CF_TEXT);
  if (hData == nullptr) {
    return {};
  }

  auto const pszText = static_cast<char *>(GlobalLock(hData));
  if (pszText == nullptr) {
    return {};
  }

  return pin + pszText;
}

int copy_username_and_password_to_anyconnect(const std::string &username, const std::string &pin) {
  click_password_copy_btn();

  auto const vpn_password_text = get_vpn_password(pin);
  if (vpn_password_text.empty()) {
    return 1;
  }

  SendMessage(vpn_username_wnd, EM_SETSEL, 0, -1);
  SendMessage(vpn_username_wnd, WM_CHAR, (WPARAM)VK_BACK, 0);

  for (char const ch : username) {
    SendMessage(vpn_username_wnd, WM_CHAR, ch, 0);
  }

  for (char const ch : vpn_password_text) {
    SendMessage(vpn_password_wnd, WM_CHAR, ch, 0);
  }

  return 0;
}

int switch_to(ConnType conn_type) {
  SendMessage(vpn_group_wnd, WM_LBUTTONDOWN, 0, 0);
  SendMessage(vpn_group_wnd, WM_LBUTTONUP, 0, 0);

  Sleep(250);

  auto const vpn_group_combo_l_box_wnd =
      find_window(nullptr, nullptr, L"ComboLBox", nullptr);
  if (vpn_group_combo_l_box_wnd == nullptr) {
    return 1;
  }

  int y_offset = 0;
  switch (conn_type) {
  case ConnType::VIP_TOKEN:
    y_offset = 2;
    break;
  case ConnType::TUNNEL_ALL:
    y_offset = 14;
    break;
  default:
    break;
  }

  SendMessage(vpn_group_combo_l_box_wnd, WM_LBUTTONDOWN, MK_LBUTTON,
              (y_offset << 16 | 40));
  SendMessage(vpn_group_combo_l_box_wnd, WM_LBUTTONUP, 0,
              (y_offset << 16 | 40));

  return 0;
}

void check_file_modifications() {
  HANDLE watch_handle = FindFirstChangeNotificationA(
      anyconnect_config_dir, FALSE,
      FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_CREATION);

  if (watch_handle == INVALID_HANDLE_VALUE || watch_handle == nullptr) {
    throw std::exception("unable to create file changes watcher");
  }

  auto status = WaitForSingleObject(watch_handle, 10000);

  if (status == WAIT_OBJECT_0) {
    if (!DeleteFileA(anyconnect_origin_config)) {
      throw std::exception("unable to delete old config");
    }
    if (!CopyFileA(anyconnect_modified_config, anyconnect_origin_config,
                   false)) {
      throw std::exception("unable to overwrite existing config");
    }
  } else if (status == WAIT_TIMEOUT) {
    throw std::exception("file watcher timed out");
  } else {
    throw std::exception("WaitForSingleObject unknown error");
  }
}

void ensure_param_exist(const char *param_name, Document &config_doc,
                        bool &modified) {
  if (!config_doc.HasMember(param_name)) {
    std::string val;
    std::cout << "Enter the value of \"" << param_name << "\" parameter: ";
    std::getline(std::cin, val);

    config_doc.AddMember(
        Value(param_name, config_doc.GetAllocator()),
        Value(val.c_str(), val.length(), config_doc.GetAllocator()),
        config_doc.GetAllocator());
    modified = true;
  }
}

std::vector<char> enc_algo(std::vector<char> data, std::string const &key) {
  if (!key.empty()) {
    for (size_t i = 0; i < data.size(); ++i)
      data[i] ^= key[i % key.size()];
  }

  return std::move(data);
}

std::vector<char> encrypt(std::string const &msg, std::string const &key) {
    return enc_algo(std::vector<char>(msg.begin(), msg.end()), key);
}

std::string decrypt(std::vector<char> data, std::string const &key) {
  auto decrypted_data = enc_algo(std::move(data), key);
  return std::string(decrypted_data.begin(), decrypted_data.end());
}

int main(int argc, char *argv[]) {
  std::fstream config_file(config_filename,
                           std::ios::in | std::ios::binary | std::ios::ate);

  std::string config_str = "{}";
  if (config_file.good()) {
    std::vector<char> file_read_buf(static_cast<size_t>(config_file.tellg()));
    config_file.seekg(0, std::ios::beg);
    config_file.read(file_read_buf.data(), file_read_buf.size());
    config_str = decrypt(std::move(file_read_buf), secret_key);
  }
  config_file.close();

  Document config_doc;
  const ParseResult parse_res = config_doc.Parse(config_str.c_str());
  if (parse_res.IsError()) {
    std::cerr << "Config parsing error: " << parse_res.Code() << std::endl;
    return 1;
  }

  bool config_edited = false;

  ensure_param_exist(param_anyconnect_executable, config_doc, config_edited);
  ensure_param_exist(param_vipaccess_executable, config_doc, config_edited);
  ensure_param_exist(param_username, config_doc, config_edited);
  ensure_param_exist(param_userpin, config_doc, config_edited);

  if (config_edited) {
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    config_doc.Accept(writer);

    config_file.open(config_filename,
                     std::ios::out | std::ios::binary | std::ios::trunc);
    auto encrypted_data = encrypt(buffer.GetString(), secret_key);
    config_file.write(encrypted_data.data(), encrypted_data.size());
    config_file.close();
  }

  ShellExecuteA(NULL, "open",
                config_doc[param_anyconnect_executable].GetString(), NULL, NULL,
                SW_SHOWNORMAL);
  ShellExecuteA(NULL, "open",
                config_doc[param_vipaccess_executable].GetString(), NULL, NULL,
                SW_SHOWNORMAL);

  ::Sleep(3000);

  if (!DeleteFileA(anyconnect_origin_config) && GetLastError() != 2) {
    std::cerr << "unable to delete old config" << std::endl;
    return 1;
  }

  std::future<void> config_update_result =
      std::async(std::launch::async, check_file_modifications);

  if (find_all_required_windows() != 0) {
    return 1;
  }

  if (argc > 1) {
    std::string conn_type(argv[1]);
    int ret = 1;
    if (conn_type == "vip_token") {
      ret = switch_to(ConnType::VIP_TOKEN);
    } else if (conn_type == "tunnel_all") {
      ret = switch_to(ConnType::TUNNEL_ALL);
    }
    if (ret != 0) {
      return ret;
    }
    Sleep(2000);
    if (find_anyconnect_connect_window() != 0) {
      return 1;
    }
  }

  if (copy_username_and_password_to_anyconnect(
          config_doc[param_username].GetString(),
          config_doc[param_userpin].GetString()) != 0) {
    return 1;
  }

  click_connect_btn();

  PostMessage(vip_access_wnd, WM_QUIT, 0, 0);

  try {
    config_update_result.get();
  } catch (std::exception const &ce) {
    std::cerr << ce.what() << std::endl;
    return 1;
  }

  return 0;
}