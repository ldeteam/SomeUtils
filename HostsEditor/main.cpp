#include "hostseditor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HostsEditor w;
    w.show();

    return a.exec();
}
