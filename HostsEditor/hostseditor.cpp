#include "hostseditor.h"
#include "ui_hostseditor.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QTextStream>
#include <QFile>


char const * const HOSTS_FILENAME = "C:\\Windows\\system32\\drivers\\etc\\hosts";


HostsEditor::HostsEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HostsEditor)
{
    ui->setupUi(this);

    QRect position = frameGeometry();
    position.moveCenter(QApplication::desktop()->availableGeometry().center());
    move(position.topLeft());

    setFixedSize(width(), height());

    QFile file(HOSTS_FILENAME);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
            ui->listWidget->addItem(line);
        }
        file.close();
    }
}

HostsEditor::~HostsEditor()
{
    delete ui;
}

void HostsEditor::on_pushButton_clicked()
{
    QFile file(HOSTS_FILENAME);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        for (int i = 0; i < ui->listWidget->count(); ++i) {
            out << ui->listWidget->item(i)->text() << "\n";
        }
        out.flush();
        file.close();
    }
}

void HostsEditor::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString text = item->text();
    item->setText((text.mid(0, 1) == "#") ? text.mid(1) : (QString("#") + text));
}
