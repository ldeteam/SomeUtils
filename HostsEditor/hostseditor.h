#ifndef HOSTSEDITOR_H
#define HOSTSEDITOR_H

#include <QMainWindow>

namespace Ui {
class HostsEditor;
}

class QListWidgetItem;

class HostsEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit HostsEditor(QWidget *parent = 0);
    ~HostsEditor();

private slots:
    void on_pushButton_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::HostsEditor *ui;
};

#endif // HOSTSEDITOR_H
