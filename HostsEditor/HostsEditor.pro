#-------------------------------------------------
#
# Project created by QtCreator 2013-10-03T18:51:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HostsEditor
TEMPLATE = app


SOURCES += main.cpp\
        hostseditor.cpp

HEADERS  += hostseditor.h

FORMS    += hostseditor.ui

win32:RC_FILE = HostsEditor.rc
